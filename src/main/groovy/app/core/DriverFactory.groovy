package app.core

import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import org.openqa.selenium.remote.DesiredCapabilities

class DriverFactory {
    private static AndroidDriver<MobileElement> driver


    static void createDriver() throws IOException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities()

        File file = new File("src/main/resources")
        File fileApp = new File(file, "speedTest.apk")

//        desiredCapabilities.setCapability("BROWSER_NAME", "chrome")
        desiredCapabilities.setCapability("fullReset", true)

        desiredCapabilities.setCapability("platformName", "Android")
        desiredCapabilities.setCapability("deviceName", "0123456789ABCDEF")
        desiredCapabilities.setCapability("platformVersion", "7.0")
        desiredCapabilities.setCapability("app", fileApp.getAbsolutePath())
        desiredCapabilities.setCapability("appPackage", "org.zwanoo.android.speedtest")
        desiredCapabilities.setCapability("appActivity", "com.ookla.mobile4.screens.main.MainActivity")
        desiredCapabilities.setCapability("automationName", "UiAutomator2")

        try {
            driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities)
        } catch (MalformedURLException e) {
            e.printStackTrace()
        }

//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
    }

    static AndroidDriver<MobileElement> getDriver() {
        if (!driver) {
            createDriver()
        }
        return driver
    }

    static void destroyDriver() {
        if (!driver) return

        try {
            driver.quit()
        } catch (Exception e) {
            e + ": AndroidDriver stop error"
        }
        driver = null
    }

}
