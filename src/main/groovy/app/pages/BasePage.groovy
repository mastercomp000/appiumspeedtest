package app.pages

import io.appium.java_client.MobileElement
import org.openqa.selenium.By

import static app.core.DriverFactory.getDriver

class BasePage {



    MobileElement method(By by) {
        return getDriver().findElement(by);
    }

    MobileElement method2(By by) {
        return getDriver().findElement(by);
    }

    MobileElement method3(String textoDoElemento) {
        return getDriver().findElement(By.xpath("//*[@text='" + textoDoElemento + "']"));
    }


}
