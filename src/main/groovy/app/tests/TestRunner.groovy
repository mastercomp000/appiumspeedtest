import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass

import static app.core.DriverFactory.destroyDriver
import static app.core.DriverFactory.getDriver

class TestRunner {
//    private BasePage basePage
//
//    BaseTest() {
//        this.basePage = new BasePage()
//    }

    @BeforeClass
    void setUpTest() throws IOException {
        getDriver()
    }

    @AfterClass
    static void finalizarDriver() {
        destroyDriver()
    }
}
